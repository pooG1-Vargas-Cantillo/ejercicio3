package edu.unimagdale.videoClub;

public class Referencia {

	private String titulo;
	private Object tipo;
	public Referencia() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Referencia(String titulo, Object tipo) {
		super();
		this.titulo = titulo;
		this.tipo = tipo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Object getTipo() {
		return tipo;
	}
	public void setTipo(Object tipo) {
		this.tipo = tipo;
	}
	
}
