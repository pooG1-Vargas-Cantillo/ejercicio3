package edu.unimagdale.videoClub;

public class RegistroAlquiler {

	private Cliente cliente;
	private Producto producto;
	private String fechaAlquiler;
	private String fechaDevolucion;
	private double importe;
	public RegistroAlquiler() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public String getFechaAlquiler() {
		return fechaAlquiler;
	}
	public void setFechaAlquiler(String fechaAlquiler) {
		this.fechaAlquiler = fechaAlquiler;
	}
	public String getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(String fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	
	public void mostrarDatos()
	{
		System.out.println("Producto: "+producto.getReferencia().getTitulo());
		System.out.println("Cliente: "+cliente.getNombre());
		System.out.println("fecha de alquiler: "+fechaAlquiler);
		System.out.println("fecha de devulucio: "+fechaDevolucion);
		System.out.println("importe:"+ importe );
	}
	
}
