package edu.unimagdale.videoClub;

public class Pelicula {

	private String nombre;
	private int a�o;
	private  String director;
	private String interpretes;
	private Generos genero;

	Pelicula(int a�o,String nombre,String director,String interpretes,Generos genero)
	{
		this.a�o = a�o;
		this.director = director;
		this.genero = genero;
		this.interpretes = interpretes;
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getInterpretes() {
		return interpretes;
	}

	public void setInterpretes(String interpretes) {
		this.interpretes = interpretes;
	}

	public Generos getGenero() {
		return genero;
	}

	public void setGenero(Generos genero) {
		this.genero = genero;
	}
	
	
}
