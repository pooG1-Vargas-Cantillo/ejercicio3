package edu.unimagdale.videoClub;

import java.util.ArrayList;

public class Producto {

	private Referencia referencia;
	private double precio;
	private int plazo;
	private boolean alquilado;
	
	public Producto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Referencia getReferencia() {
		return referencia;
	}
	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getPlazo() {
		return plazo;
	}
	public void setPlazo(int plazo) {
		this.plazo = plazo;
	}
	public boolean isAlquilado() {
		return alquilado;
	}
	public void setAlquilado(boolean alquilado) {
		this.alquilado = alquilado;
	}
	public void mostrarDatos() {
		System.out.println("Producto: "+referencia.getTitulo());
		if(referencia.getTipo() instanceof Pelicula)
			System.out.println("Tipo: Pelicula");
		else
			System.out.println("Tipo: Videojuego");
		
		System.out.println("Precio: "+precio);
		System.out.println("Plazo: "+plazo);
		String a = alquilado? "Si":"No";
		System.out.println("Alquilado?: "+a );
	}
}
