package edu.unimagdale.videoClub;

import java.util.ArrayList;

public class Cliente {

	private long id;
	private String nombre;
	private String direccion;
	private String telefono;
	private ArrayList<Producto> productos;
	public Cliente() {
		productos = new ArrayList<Producto>();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public ArrayList<Producto> getProductos() {
		return productos;
	}
	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	
	public void agregarProducto(Producto p){
		this.productos.add(p);
	}
	
	public void mostrarDatos() {
		System.out.println("Cliente [id=" + id + ", nombre=" + nombre + ", direccion="
				+ direccion + ", telefono=" + telefono);
		for(Producto p: productos){
			p.mostrarDatos();
		}
		
		
	}
}
